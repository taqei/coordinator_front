$(function () {

  'use strict';

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  // -----------------------
  // - MONTHLY SALES CHART -
  // -----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
  // This will get the first returned node in the jQuery collection.
  var salesChart       = new Chart(salesChartCanvas);

  var salesChartData = {
    labels  : ['01/June/2019', '02/June/2019', '03/June/2019', '04/June/2019', '05/June/2019', '06/June/2019', '07/June/2019'],
    datasets: [
      {
        label               : 'Europe',
        strokeColor           : '#d33724',
        pointHighlightFill: '#d33724',
        data                : Array.from({length: 7}, () => Math.floor(Math.random()*(100-40+1)+40))
      },
      {
        label               : 'Asia',
        strokeColor           : '#00a65a',
        pointHighlightFill: '#00a65a',
        data                : Array.from({length: 7}, () => Math.floor(Math.random()*(100-40+1)+40))
      },
      {
        label               : 'Africa',
        strokeColor          : '#f39c12',
        pointHighlightFill: '#f39c12',
        data                : Array.from({length: 7}, () => Math.floor(Math.random()*(100-40+1)+40))
      },
      {
        label               : 'America',
        strokeColor           : '#00c0ef',
        pointHighlightFill:  '#00c0ef',
        data                : Array.from({length: 7}, () => Math.floor(Math.random()*(100-40+1)+40))
      },
    ]
  };

  var salesChartOptions = {
    // Boolean - If we should show the scale at all
    showScale               : true,
    // Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    // String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    // Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    // Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    // Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    // Boolean - Whether the line is curved between points
    bezierCurve             : true,
    // Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    // Boolean - Whether to show a dot for each point
    pointDot                : false,
    // Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    // Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    // Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    // Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    // Boolean - Whether to fill the dataset with a color
    datasetFill             : false,
    // String - A legend template
    legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : true,
    // Boolean - whether to make the chart responsive to window resizing
    responsive              : true,
    scaleStartValue : 0 
  };

  // Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);

  // ---------------------------
  // - END MONTHLY SALES CHART -
  // ---------------------------

  // -------------
  // - PIE CHART -
  // -------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
  var pieChart       = new Chart(pieChartCanvas);
  var PieData        = [
    {
      value    : 7,
      color    : '#f56954',
      highlight: '#f56954',
      label    : 'Europe'
    },
    {
      value    : 5,
      color    : '#00a65a',
      highlight: '#00a65a',
      label    : 'Asia'
    },
    {
      value    : 4,
      color    : '#f39c12',
      highlight: '#f39c12',
      label    : 'Africa'
    },
    {
      value    : 6,
      color    : '#00c0ef',
      highlight: '#00c0ef',
      label    : 'America'
    },
    {
      value    : 3,
      color    : '#3c8dbc',
      highlight: '#3c8dbc',
      label    : 'Australia'
    },
    {
      value    : 1,
      color    : '#d2d6de',
      highlight: '#d2d6de',
      label    : 'Antarctia'
    }
  ];
  var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%> VNFs'
  };
  // Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  // -----------------
  // - END PIE CHART -
  // -----------------

  /* jVector Maps
   * ------------
   * Create a world map with markers
   */
  var markers = [
    { latLng: [41.90, 12.45], name: 'Vatican City', idd :2 },
    { latLng: [43.73, 7.41], name: 'Monaco',idd : 1 }
  ];
  $('#world-map-markers').vectorMap({
    map              : 'world_mill_en',
    normalizeFunction: 'polynomial',
    hoverOpacity     : 0.7,
    hoverColor       : false,
    backgroundColor  : 'transparent',
    regionStyle      : {
      initial      : {
        fill            : 'rgba(210, 214, 222, 1)',
        'fill-opacity'  : 1,
        stroke          : 'none',
        'stroke-width'  : 0,
        'stroke-opacity': 1
      },
      hover        : {
        'fill-opacity': 0.7,
        cursor        : 'pointer'
      },
      selected     : {
        fill: 'yellow'
      },
      selectedHover: {fill: 'yellow'}
    },
    markerStyle      : {
      initial: {
        fill  : '#00a65a',
        stroke: '#111'
      }
    },
    markers          : markers,
    onMarkerLabelShow: function(event, label, code) {

   
      var info = {
        name :"hhhh",
        vnf_type : "cache",
        cloud : "openstack",
        vnf_location : "xxx",
        cpu : [10,15,1,50,50,70,60],
        storage : 0.87
      };

      $(label).css("background-color", "#fff");
      var tt = 
        '<div style="color:black"> \
          <h4 > <b>Name</b> : '+info["name"]+'</h4> \
          <h5 > <b>VNF type</b> : '+info["vnf_type"]+'</h5> \
          <h6 > <b>Infra</b> : '+info["cloud"]+'</h6> \
          <h6 > <b>Location</b> : '+info["vnf_location"]+'</h6> \
          <span class="sparkline-1">10,8,9,3,5,8,5</span> \
        </div>';

        
   
      $(label).append(tt);
      $('.sparkline-1').sparkline([1, 3, 5, 3, 8], {
        type: 'bar',
        tooltipFormat: '{{value:levels}} - {{value}}',
        tooltipValueLookups: {
          levels: $.range_map({':2': 'Low', '3:6': 'Medium', '7:': 'High'})
        }
      });
  
    }
  });

  /* SPARKLINE CHARTS
   * ----------------
   * Create a inline charts with spark line
   */

  // -----------------
  // - SPARKLINE BAR -
  // -----------------
  $('.sparkbar').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type    : 'bar',
      height  : $this.data('height') ? $this.data('height') : '30',
      barColor: $this.data('color')
    });
    console.log("hhhhh");
  });

  

  // -----------------
  // - SPARKLINE PIE -
  // -----------------
  $('.sparkpie').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type       : 'pie',
      height     : $this.data('height') ? $this.data('height') : '90',
      sliceColors: $this.data('color')
    });
  });

  // ------------------
  // - SPARKLINE LINE -
  // ------------------
  $('.sparkline').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type     : 'line',
      height   : $this.data('height') ? $this.data('height') : '90',
      width    : '100%',
      lineColor: $this.data('linecolor'),
      fillColor: $this.data('fillcolor'),
      spotColor: $this.data('spotcolor')
    });
  });
});
